package Kakfa_Consumer;

import com.rabbitmq.client.*;
import java.io.IOException;


public class ConsumerRabbit {

    private ConnectionFactory factory;
    private Connection  connection;
    private Channel channel;


    public ConsumerRabbit(){

        factory = new ConnectionFactory();
            factory.setHost( Config.RABBITMQ_SERVER );
            factory.setVirtualHost( Config.RABBITMQ_VIRTUALHOST );
            factory.setPort( Config.RABBITMQ_PORT );
            factory.setUsername( Config.USERNAME );
            factory.setPassword( Config.PASSWORD );
     }

     public Channel Create(){

         try {
                    connection = factory.newConnection();
                    channel  = connection.createChannel();
                    channel.exchangeDeclare(Config.QUEUE_NAME, "topic");

            } catch (Exception  e) {
                        e.printStackTrace();
                }
         return channel;
         }

         public void sendMessage(String message){
                            try {
                    channel.basicPublish("", Config.QUEUE_NAME, null, message.getBytes());
                              System.out.println( "Sent message '" + message + "'" );
             } catch (IOException e) {
                 e.printStackTrace();
             }
         }

         public void receiveMesssage() {
                Consumer consumer = new DefaultConsumer(channel) {
                  @Override
                     public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                        String message = new String(body);
                         System.out.println("Message Received! ->  " + message + envelope.getClass()+ "(" + envelope.getRoutingKey() + ", " + envelope.getDeliveryTag() + ")");
                 }};
                    try {
                        channel.basicConsume(Config.QUEUE_NAME, true, consumer);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

         public void close() {
             try {
                 channel.close();
                 connection.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                        }
                    }

     public static void main(String[] args) throws IOException {
         ConsumerRabbit connection= new ConsumerRabbit();
         connection.Create();
         connection.sendMessage(Config.QUEUE_NAME);
         connection.receiveMesssage();
      }
}





